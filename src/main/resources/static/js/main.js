'use strict'

var tweets = document.querySelector('#tweets')
var errors = document.querySelector('#errors')

var stompClient=null;
var socket=null;

function connect() {
    socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, onConnected, onConnectionError);
}

function onConnected() {
    stompClient.subscribe('/tweets', onMessageReceived)
    stompClient.subscribe('/tweets/error', onErrorReceived)
}

function onConnectionError(error) {
    addToErrorsList(error);
}

function onErrorReceived(error) {
    addToErrorsList(JSON.parse(error.body).message);
}

function addToErrorsList(message) {
    var errorElement = document.createElement('li');
    var errorTextElement = document.createTextNode(message);

    errorElement.appendChild(errorTextElement);
    errors.appendChild(errorElement);
}

function onMessageReceived(payload) {
    var tweetText = payload.body;

    var tweetElement = document.createElement('li');
    var tweetTextElement = document.createTextNode(tweetText);

    while(tweets.childElementCount >= 10) {
        tweets.removeChild(tweets.lastChild);
    }

    tweetElement.appendChild(tweetTextElement);
    tweets.insertBefore(tweetElement, tweets.firstChild);
}

window.onload = connect();