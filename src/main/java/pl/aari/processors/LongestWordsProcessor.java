package pl.aari.processors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@Component
public class LongestWordsProcessor implements MessageProcessor {

    private static final int DEFAULT_WORDS_NUMBER = 5;

    private int allowedWordsNumber = DEFAULT_WORDS_NUMBER;

    @Override
    public String process(String message) {
        if (isEmpty(message)) {
            return message;
        }

        String[] words = StringUtils.split(message);

        TreeMap<Integer, Set<String>> wordsGroupedByLength = new TreeMap<>((l1, l2)
                -> Integer.compare(l2, l1));

        Arrays.stream(words).forEach(word -> {
            if (!wordsGroupedByLength.containsKey(word.length())) {
                wordsGroupedByLength.put(word.length(), new LinkedHashSet<>());
            }
            wordsGroupedByLength.get(word.length()).add(word);
        });

        List<String> resultWords = new LinkedList<>();
        for (Map.Entry<Integer, Set<String>> wordSet : wordsGroupedByLength.entrySet()) {
            if (resultWords.size() >= allowedWordsNumber) {
                break;
            }
            resultWords.addAll(wordSet.getValue());
        }

        return resultWords.stream().collect(Collectors.joining(" "));
    }

    @Override
    public int getOrder() {
        return 2;
    }

    public void setAllowedWordsNumber(int allowedWordsNumber) {
        this.allowedWordsNumber = allowedWordsNumber;
    }
}
