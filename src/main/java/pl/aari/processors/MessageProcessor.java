package pl.aari.processors;

import pl.aari.model.Tweet;

import java.io.Serializable;

public interface MessageProcessor {

    String process(String message);

    int getOrder();
}
