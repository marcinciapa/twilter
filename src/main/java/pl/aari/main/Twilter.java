package pl.aari.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"pl.aari.configuration", "pl.aari.processors"})
public class Twilter {

    public static void main(String[] args) {
        SpringApplication.run(Twilter.class, args);
    }
}
