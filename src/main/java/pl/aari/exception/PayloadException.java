package pl.aari.exception;

public class PayloadException extends Exception {

    public PayloadException(String message) {
        super(message);
    }
}
