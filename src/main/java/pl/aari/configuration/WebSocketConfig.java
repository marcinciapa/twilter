package pl.aari.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import pl.aari.buffer.TweetsBuffer;
import pl.aari.processors.MessageProcessor;
import pl.aari.twitterclient.TwitterClient;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Autowired
    TwitterClient client;

    @Autowired
    MessageProcessor messageProcessor;

    @Autowired
    private SimpMessageSendingOperations messaging;

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws").withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/tweets");

        TweetsBuffer buffer = new TweetsBuffer();

        TwitterClient.OnMessageReceivedCallback onMessageReceivedCallback = (tweet) -> {


            buffer.processAnotherTweet(tweet.getText(), (value) -> {
                messaging.convertAndSend(
                        "/tweets",
                        messageProcessor.process(value)
                );
            });

        };

        TwitterClient.OnErrorCallback onErrorCallback = (error) ->
                messaging.convertAndSend("/tweets/error", error);

        client.collectData(onMessageReceivedCallback, onErrorCallback);
    }
}
