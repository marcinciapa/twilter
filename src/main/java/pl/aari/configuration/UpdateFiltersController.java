package pl.aari.configuration;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.aari.exception.PayloadException;
import pl.aari.processors.LongestWordsProcessor;
import pl.aari.twitterclient.TwitterClient;

@Controller
public class UpdateFiltersController {

    @Autowired
    private TwitterClient twitterClient;

    @Autowired
    private LongestWordsProcessor processor;

    @GetMapping("/")
    public String serveIndex() {
        return "index";
    }

    @PostMapping("/")
    public String updateFilters(
            @RequestParam String trackTerm,
            @RequestParam(required = false) Integer wordsCount,
            Model model)
            throws PayloadException {

        if (StringUtils.isEmpty(trackTerm)) {
            throw new PayloadException("Filter word cannot be empty.");
        }
        if (wordsCount != null) {
            if (wordsCount < 0) {
                throw new PayloadException("Words count must be a positive number");
            }
            processor.setAllowedWordsNumber(wordsCount);
        }
        twitterClient.updateFilters(trackTerm);
        return "index";
    }

    @ExceptionHandler(PayloadException.class)
    public String onError(Model model, Exception e) {
        model.addAttribute("errorMessage", e.getMessage());
        return "error";
    }
}
