package pl.aari.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import pl.aari.twitterclient.TwitterClient;
import pl.aari.twitterclient.credentials.TwitterCredentials;
import pl.aari.twitterclient.hosebird.HosebirdClient;
import pl.aari.twitterclient.hosebird.HosebirdClientSupplier;

@Configuration
@PropertySource(
        value = "classpath:twilter.properties"
)
public class TwitterClientConfig {

    @Autowired
    private Environment twilterEnvironment;

    @Bean
    @Autowired
    TwitterClient twitterClient(HosebirdClientSupplier hosebirdClientSupplier) {
        return new HosebirdClient(hosebirdClientSupplier);
    }

    @Bean
    @Autowired
    HosebirdClientSupplier hosebirdClientSupplier(TwitterCredentials twitterCredentials) {
        return new HosebirdClientSupplier(twitterCredentials);
    }

    @Bean
    TwitterCredentials twitterCredentials() {
        String consumerKey = twilterEnvironment.getProperty("consumerKey");
        String consumerSecret = twilterEnvironment.getProperty("consumerSecret");
        String token = twilterEnvironment.getProperty("token");
        String tokenSecret = twilterEnvironment.getProperty("tokenSecret");

        return new TwitterCredentials(consumerKey, consumerSecret, token, tokenSecret);
    }
}
