package pl.aari.buffer;

import java.util.LinkedList;
import java.util.stream.Collectors;

public class TweetsBuffer {

    private LinkedList<String> buffer = new LinkedList<>();

    public synchronized void processAnotherTweet(String message, OnDesiredSizeAccuired callback) {
        buffer.add(message);
        if (buffer.size() >= 10) {
            callback.call(read());
            buffer.removeFirst();
        }
    }

    public interface OnDesiredSizeAccuired {

        void call(String value);
    }

    private String read() {
        return buffer.stream().collect(Collectors.joining(" "));
    }

}
