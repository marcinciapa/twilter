package pl.aari.twitterclient.hosebird;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twitter.hbc.httpclient.BasicClient;
import pl.aari.model.Error;
import pl.aari.model.Tweet;
import pl.aari.twitterclient.TwitterClient;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class HosebirdClient implements TwitterClient {

    private final HosebirdClientSupplier clientSupplier;
    private BasicClient hosebirdClient;
    private final BlockingQueue<String> messagesQueue;
    private OnMessageReceivedCallback onMessageReceived;
    private OnErrorCallback onErrorCallback;

    public HosebirdClient(HosebirdClientSupplier clientSupplier) {
        this.clientSupplier = clientSupplier;
        messagesQueue = new LinkedBlockingQueue<>(100000);
        hosebirdClient = clientSupplier.supply(DEFAULT_TRACK_TERM, messagesQueue);
        hosebirdClient.connect();
    }

    @Override
    public void collectData(
            OnMessageReceivedCallback onMessageReceived,
            OnErrorCallback onErrorCallback) {

        this.onMessageReceived = onMessageReceived;
        this.onErrorCallback = onErrorCallback;

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            while (true) {
                try {
                    String message = messagesQueue.take();
                    ObjectMapper mapper = new ObjectMapper();
                    Tweet tweet = mapper.readValue(message, Tweet.class);
                    if (onMessageReceived != null) {
                        this.onMessageReceived.call(tweet);
                    }
                } catch (Exception e) {
                    if (onErrorCallback != null) {
                        Error error = new Error();
                        error.setMessage("Error occurred during fetching tweet.");
                        onErrorCallback.call(error);
                    }
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void updateFilters(String trackTerms) {
        if (hosebirdClient != null) {
            hosebirdClient.stop();
        }
        hosebirdClient = clientSupplier.supply(trackTerms, messagesQueue);
        hosebirdClient.connect();

        collectData(onMessageReceived, onErrorCallback);
    }
}
