package pl.aari.twitterclient.hosebird;

import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.BasicClient;
import com.twitter.hbc.httpclient.auth.OAuth1;
import pl.aari.twitterclient.credentials.TwitterCredentials;

import java.util.Collections;
import java.util.concurrent.BlockingQueue;

public class HosebirdClientSupplier {

    private final TwitterCredentials credentials;

    public HosebirdClientSupplier(TwitterCredentials credentials) {
        this.credentials = credentials;
    }

    public BasicClient supply(String trackTerms, BlockingQueue<String> messagesQueue) {
        HttpHosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
        StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
        hosebirdEndpoint.trackTerms(Collections.singletonList(trackTerms));
        OAuth1 authentication = new OAuth1(
                credentials.getConsumerKey(),
                credentials.getConsumerSecret(),
                credentials.getToken(),
                credentials.getTokenSecret());

        ClientBuilder hosebirdClient = new ClientBuilder()
                .hosts(hosebirdHosts)
                .endpoint(hosebirdEndpoint)
                .authentication(authentication)
                .processor(new StringDelimitedProcessor(messagesQueue));

        return hosebirdClient.build();
    }
}
