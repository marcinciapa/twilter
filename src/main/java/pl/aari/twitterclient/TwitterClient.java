package pl.aari.twitterclient;

import pl.aari.model.Error;
import pl.aari.model.Tweet;

public interface TwitterClient {

    String DEFAULT_TRACK_TERM = "USA";

    void collectData(
            OnMessageReceivedCallback onMessageReceived,
            OnErrorCallback onErrorCallback
    );

    void updateFilters(String trackTerms);

    interface OnMessageReceivedCallback {
        void call(Tweet message);
    }

    interface OnErrorCallback {
        void call(Error error);
    }
}
