package pl.aari.processors;

import org.junit.Test;
import pl.aari.model.Tweet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class LongestWordsProcessorTest {

    @Test
    public void shouldReturnNullForNullTweet() {
        // given
        LongestWordsProcessor processor = new LongestWordsProcessor();

        // when
        String processed = processor.process(null);

        // then
        assertNull(processed);
    }

    @Test
    public void shouldReturnEmptyMessageForEmptyTweet() {
        // given
        Tweet tweet = new Tweet();
        tweet.setText("");
        LongestWordsProcessor processor = new LongestWordsProcessor();

        // when
        String processed = processor.process("");

        // then
        assertEquals("", processed);
    }

    @Test
    public void shouldReturnExactNumberOfWordsGroupedByLength() {
        // given
        LongestWordsProcessor processor = new LongestWordsProcessor();
        Tweet tweet = new Tweet();
        tweet.setText("aa bbb dddd ccccc ccccc");
        processor.setAllowedWordsNumber(3);

        // when
        String processed = processor.process(tweet.getText());

        // then
        assertEquals("ccccc dddd bbb", processed);
    }

    @Test
    public void shouldReturnExtendedNumberOfWordsGroupedByLength() {
        // given
        LongestWordsProcessor processor = new LongestWordsProcessor();
        Tweet tweet = new Tweet();
        tweet.setText("z bbb aaa dddd ccccc ccccc");
        processor.setAllowedWordsNumber(3);

        // when
        String processed = processor.process(tweet.getText());

        // then
        assertEquals("ccccc dddd bbb aaa", processed);

    }

    @Test
    public void shouldReturnSmallerNumberOfWordsGroupedByLength() {
        // given
        LongestWordsProcessor processor = new LongestWordsProcessor();
        Tweet tweet = new Tweet();
        tweet.setText("z bbb aaa dddd ccccc ccccc");
        processor.setAllowedWordsNumber(10);

        // when
        String processed = processor.process(tweet.getText());

        // then
        assertEquals("ccccc dddd bbb aaa z", processed);
    }

}