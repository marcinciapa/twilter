package pl.aari.buffer;

import org.junit.Test;
import org.mockito.Mockito;

import static pl.aari.buffer.TweetsBuffer.OnDesiredSizeAccuired;

public class TweetsBufferTest {

    private TweetsBuffer buffer = new TweetsBuffer();


    @Test
    public void shouldConcatTweetsUntil10th() {

        OnDesiredSizeAccuired callback = Mockito.mock(OnDesiredSizeAccuired.class);

        TweetsBuffer tweetsBuffer = new TweetsBuffer();

        tweetsBuffer.processAnotherTweet("1", callback);
        tweetsBuffer.processAnotherTweet("1", callback);
        tweetsBuffer.processAnotherTweet("1", callback);
        tweetsBuffer.processAnotherTweet("1", callback);
        tweetsBuffer.processAnotherTweet("1", callback);
        tweetsBuffer.processAnotherTweet("1", callback);
        tweetsBuffer.processAnotherTweet("1", callback);
        tweetsBuffer.processAnotherTweet("1", callback);
        tweetsBuffer.processAnotherTweet("1", callback);
        tweetsBuffer.processAnotherTweet("1", callback);

        Mockito.verify(callback).call("1 1 1 1 1 1 1 1 1 1");
        Mockito.verifyNoMoreInteractions(callback);

    }

}